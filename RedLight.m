//
//  RedLight.m
//  RedLightNotifier
//
//  Created by Maxwell Elliott on 3/19/13.
//  Copyright (c) 2013 Maxwell Elliott. All rights reserved.
//

#import "RedLight.h"
@implementation RedLight
@synthesize title,coordinate,subtitle,location;
-(id)initWithJSONData:(NSData *)jsonData location:(CLLocation *)loc
{
    self = [super init];
    if (self) {
        title = [jsonData valueForKey:@"name"];
        state = [jsonData valueForKey:@"state"];
        city = [jsonData valueForKey:@"city"];
 
        CLLocationDegrees latitude = [[jsonData valueForKey:@"latitude"] floatValue];
        CLLocationDegrees longitude = [[jsonData valueForKey:@"longitude"] floatValue];
        coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        CLLocationDistance distanceBetweenLightAndUser = [loc distanceFromLocation:location];
        float distanceInMiles = distanceBetweenLightAndUser *  0.000621371192;
        subtitle = [NSString stringWithFormat:@"%@, %@, %.2f miles",city,state,distanceInMiles];
    }
    return self;
}
- (BOOL)isEqual:(RedLight *)other {
    if (other == self)
        return YES;
    if (!other || ![other isKindOfClass:[self class]])
        return NO;
    if (![self.title isEqualToString:other.title])
        return NO;
    return YES;
}
-(NSString *)generateNewSubtitleForLocation:(CLLocation *)loc{
    CLLocationDistance distanceBetweenLightAndUser = [loc distanceFromLocation:location];
    float distanceInMiles = distanceBetweenLightAndUser *  0.000621371192;
    return [NSString stringWithFormat:@"%@, %@, %.2f miles",city,state,distanceInMiles];
}

@end
