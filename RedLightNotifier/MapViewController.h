//
//  MapViewController.h
//  RedLightNotifier
//
//  Created by Maxwell Elliott on 3/3/13.
//  Copyright (c) 2013 Maxwell Elliott. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "AFNetworking.h"
#import "RedLight.h"
#import <iAd/iAd.h>
@class AFHTTPClient;
@interface MapViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate,ADBannerViewDelegate>{
    IBOutlet MKMapView *mapView;
    CLLocationManager *locationManager;
    AFHTTPClient *redlightApiClient;
    CLLocation *initialLoc;
    __weak IBOutlet UIToolbar *toolbar;
    UIBarButtonItem *toolbarTextButton;
    NSMutableDictionary *recentAlerts;
    BOOL bannerIsVisible;
    ADBannerView *bannerView;
}
@property (nonatomic, assign) BOOL bannerIsVisible;

-(void)stopLocationManager;
-(void)reduceLocationManagerAccuracy;
-(void)increaseLocationManagerAccuracy;
@end
