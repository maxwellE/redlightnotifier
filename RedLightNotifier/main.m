//
//  main.m
//  RedLightNotifier
//
//  Created by Maxwell Elliott on 3/3/13.
//  Copyright (c) 2013 Maxwell Elliott. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RedLightAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RedLightAppDelegate class]));
    }
}
