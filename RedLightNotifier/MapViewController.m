//
//  MapViewController.m
//  RedLightNotifier
//
//  Created by Maxwell Elliott on 3/3/13.
//  Copyright (c) 2013 Maxwell Elliott. All rights reserved.
//

#import "MapViewController.h"
#import "MBProgressHUD.h"

@interface MapViewController ()

@end

const double UPDATE_DISTANCE = 8046;
const double ALERT_DISTANCE = 300.0;

@implementation MapViewController

@synthesize bannerIsVisible;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    initialLoc = nil;
    // Custom initialization
    if (redlightApiClient == nil) {
        redlightApiClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://frozen-bayou-5871.herokuapp.com/"]];
    }
    if (locationManager == nil) {
        locationManager = [[CLLocationManager alloc] init];
    }
    recentAlerts = [[NSMutableDictionary alloc] init];
    [locationManager setDelegate:self];
    [locationManager setActivityType:CLActivityTypeAutomotiveNavigation];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    [locationManager setDistanceFilter:100];
    [locationManager startUpdatingLocation];
    [mapView setShowsUserLocation:YES];
    [mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    MKUserTrackingBarButtonItem *trackButton = [[MKUserTrackingBarButtonItem alloc] initWithMapView:mapView];
    toolbarTextButton = [[UIBarButtonItem alloc]initWithTitle:@"RedLightNotifier" style:UIBarButtonItemStylePlain target:nil action:nil];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [spacer setWidth:-6];
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshRedLightCameras:)];
    [refreshButton setStyle:UIBarButtonItemStyleBordered];
    [toolbar setItems:[NSArray arrayWithObjects:trackButton,spacer,toolbarTextButton,spacer,refreshButton,nil] animated:YES];
    self.canDisplayBannerAds = YES;
}

-(void)reduceLocationManagerAccuracy
{
    [locationManager startMonitoringSignificantLocationChanges];
}

-(void)increaseLocationManagerAccuracy
{
    [locationManager startUpdatingLocation];
}

-(IBAction) refreshRedLightCameras:(id)sender
{
    [self callApiForAnnotationsWithLocation: [[mapView userLocation] location] remove:NO];
}


- (void)sendNotificationAndAlertForRegionTitle:(NSString *)title {
    UILocalNotification *redLightNotification = [[UILocalNotification alloc] init];
    redLightNotification.alertBody = [NSString stringWithFormat:@"Watch out! you are nearing a Red Light Camera at \"%@\"", title];
    redLightNotification.alertAction = @"Nearing Red Light Camera!";
    redLightNotification.hasAction = NO;
    if (![[[UIApplication sharedApplication] scheduledLocalNotifications] containsObject:redLightNotification] || [[[UIApplication sharedApplication] scheduledLocalNotifications] count] == 0) {
        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
                     [[UIApplication sharedApplication] presentLocalNotificationNow:redLightNotification];
        }else{
            UIAlertView *redLightAlert = [[UIAlertView alloc] initWithTitle:@"Nearing Red Light Camera!" message:[NSString stringWithFormat:@"Watch out! you are nearing a Red Light Camera at \"%@\"", title] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Okay", nil];
            [redLightAlert show];
        }
    }
}
- (void)callApiForAnnotationsWithLocation:(CLLocation *)loc remove:(BOOL)removeAnnotations
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setAnimationType:MBProgressHUDAnimationFade];
    [hud setLabelText:@"Loading Camera Data"];
    for (id annotation in mapView.annotations) {
        if ([annotation isKindOfClass:[RedLight class]]) {
            if (removeAnnotations) {
                [mapView removeAnnotation:annotation];
            }
        }
    }
    NSURLRequest *request = [redlightApiClient requestWithMethod:@"GET" path:@"in_proximity_of" parameters:[NSDictionary dictionaryWithObjectsAndKeys:@"10",@"distance_in_miles",[NSString stringWithFormat:@"%f",loc.coordinate.latitude],@"latitude", [NSString stringWithFormat:@"%f",loc.coordinate.longitude],@"longitude", nil]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSArray *json) {
                                             int counter = 0;
                                             for (NSData *light in json) {
                                                 if (![mapView.annotations containsObject:[[RedLight alloc]initWithJSONData:light location:loc]]) {
                                                     RedLight *newLight = [[RedLight alloc]initWithJSONData:light location:loc];
                                                                                                      [mapView addAnnotation:newLight];
                                                     counter++;
                                                 }
                                             }
                                             if (loc) {
                                                 [self issueAlertsToUserForLocationUpdates:[NSArray arrayWithObject:loc]];
                                             }
                                             [hud hide:YES];
                                             if (removeAnnotations) {
                                                                                              [toolbarTextButton setTitle:[NSString stringWithFormat:@"%d cameras in vicinity", counter]];
                                             }


                                         }
                                     failure:^( NSURLRequest *request , NSHTTPURLResponse *response , NSError *error , id JSON ){
                                             [hud setLabelText:@"Failed to contact server"];
                                             [hud hide:YES afterDelay:3.0];
                                         }];
    [operation start];
}
- (void)issueAlertsToUserForLocationUpdates:(NSArray *)locations {
    for (id <MKAnnotation>annotation in mapView.annotations) {
        if([annotation isKindOfClass:[RedLight class]] && [[[CLLocation alloc] initWithLatitude:[annotation coordinate].latitude longitude:[annotation coordinate].longitude] distanceFromLocation:locations.lastObject] < ALERT_DISTANCE){
            if ([recentAlerts objectForKey:[annotation title]]) {
                NSTimeInterval distanceBetweenDates;
                NSDate *lastFire = [recentAlerts objectForKey:[annotation title]];
                distanceBetweenDates = [[NSDate date] timeIntervalSinceDate:lastFire];
                if (distanceBetweenDates > 600) {
                    [self sendNotificationAndAlertForRegionTitle:[annotation title]];
                    [recentAlerts setObject:[NSDate date] forKey:[annotation title]];
                }
            } else {
                [self sendNotificationAndAlertForRegionTitle:[annotation title]];
                [recentAlerts setObject:[NSDate date] forKey:[annotation title]];
            }
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    if (mapView.annotations.count == 0) {
        [mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    }
    [self issueAlertsToUserForLocationUpdates:locations];
}

-(void)mapView:(MKMapView *)mapViewNew didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if (initialLoc == nil || [initialLoc distanceFromLocation:[userLocation location]] > UPDATE_DISTANCE) {
        initialLoc = [userLocation location];
        [self callApiForAnnotationsWithLocation:[userLocation location] remove:YES];
        [recentAlerts removeAllObjects];
    }
    if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
    {
        for (id light in mapViewNew.annotations) {
            if ([light isKindOfClass:[RedLight class]]) {
                [light performSelectorInBackground:@selector(setSubtitle:) withObject:[light generateNewSubtitleForLocation:[locationManager location]]];
            }
        }
    }
}

-(MKAnnotationView *)mapView:(MKMapView *)mapViewAnnotation viewForAnnotation:(id<MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[RedLight class]])
    {
        // Try to dequeue an existing pin view first.
        MKAnnotationView* pinView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"RedLight"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                       reuseIdentifier:@"RedLight"];
            pinView.canShowCallout = YES;
            pinView.image = [UIImage imageNamed:@"marker.png"];
        }
        else
            pinView.annotation = annotation;
        
        return pinView;
    }
    return nil;
}

-(void) stopLocationManager
{
    [locationManager stopUpdatingLocation];
    [locationManager stopMonitoringSignificantLocationChanges];
    [locationManager setDelegate:nil];
    locationManager = nil;
}

@end
