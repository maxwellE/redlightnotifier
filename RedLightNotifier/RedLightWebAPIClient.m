//
//  RedLight.m
//  RedLightNotifier
//
//  Created by Maxwell Elliott on 3/5/13.
//  Copyright (c) 2013 Maxwell Elliott. All rights reserved.
//

#import "RedLightWebAPIClient.h"
@implementation RedLightWebAPIClient

+ (id)sharedInstance {
    static RedLightWebAPIClient *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[RedLightWebAPIClient alloc] initWithBaseURL:
                            [NSURL URLWithString:@"http://redlights.herokuapp.com/"]];
    });
    
    return __sharedInstance;
}
- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
        //custom settings
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    }
    
    return self;
}

@end
