//
//  RedLightAppDelegate.h
//  RedLightNotifier
//
//  Created by Maxwell Elliott on 3/3/13.
//  Copyright (c) 2013 Maxwell Elliott. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MapViewController;
@interface RedLightAppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MapViewController *mvc;
@end
