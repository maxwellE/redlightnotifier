//
//  RedLight.h
//  RedLightNotifier
//
//  Created by Maxwell Elliott on 3/5/13.
//  Copyright (c) 2013 Maxwell Elliott. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
@interface RedLightWebAPIClient : AFHTTPClient

+ (id)sharedInstance;

@end
