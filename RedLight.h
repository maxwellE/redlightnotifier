//
//  RedLight.h
//  RedLightNotifier
//
//  Created by Maxwell Elliott on 3/19/13.
//  Copyright (c) 2013 Maxwell Elliott. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface RedLight : NSObject <MKAnnotation>
{
    NSString *state;
    NSString *city;
}
-(id)initWithJSONData:(NSData *)jsonData location:(CLLocation *)loc;
// This is a required property from MKAnnotation
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

// This is an optional property from MKAnnotation
@property (nonatomic, copy) NSString *title;

@property (nonatomic,copy) NSString *subtitle;

@property (nonatomic, copy) CLLocation *location;

-(NSString *)generateNewSubtitleForLocation:(CLLocation *)loc;

@end
